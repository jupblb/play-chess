var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	entry: "./src/main",

	output: {
		path: __dirname + "/build/",
		filename: "app.js"
	},

	plugins: [
		new ExtractTextPlugin('style.css', {allChunks: true})
	],

	module: {
		loaders: [
			{
				test: /\.json$/,
				loader: "json-loader"
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: "babel-loader?optional[]=runtime&stage=0"
			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract('style-loader', [
					'css-loader?modules&localIdentName=[hash:base64:5]',
					'postcss-loader'
				].join('!'))
			},
			{
				test: /\.jpe?g$|\.gif$|\.png$|\.ico|\.svg$|\.woff$|\.ttf$/,
				loader: 'file-loader?name=[path][name].[ext]'
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('style-loader', [
					'css-loader?modules&localIdentName=[hash:base64:5]',
					'postcss-loader',
					'sass-loader?outputStyle=expanded'
				].join('!'))
			}
		]
	},

	resolve: {
		extensions: ['', '.js', '.jsx', '.css']
	},

	postcss: [
		require('autoprefixer'),
		require('postcss-nested')
	]
};
