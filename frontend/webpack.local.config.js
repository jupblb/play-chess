var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

    devtool: "eval",

    entry: [
        "webpack-dev-server/client?http://localhost:9090",
        "webpack/hot/only-dev-server",
        "./src/main"
    ],

    output: {
        path: __dirname + "/build/",
        filename: "app.js",
        publicPath: "http://localhost:9090/build/"
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new ExtractTextPlugin('style.css', {allChunks: true})
    ],

    module: {
        loaders: [
            {
                test: /\.json$/,
                loader: "json-loader"
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ["react-hot", "babel-loader?optional[]=runtime&stage=0"]
            },
            {
                test: /\.css$/,
                loader: [
                    'style-loader',
                    'css-loader?sourceMap&modules&localIdentName=[name]__[local]___[hash:base64:5]',
                    'postcss-loader'
                ].join('!')
            },
            {
                test: /\.jpe?g$|\.gif$|\.png$|\.ico|\.svg$|\.woff$|\.ttf$/,
                loader: 'file-loader?name=[path][name].[ext]'
            },
            {
                test: /\.scss$/,
                loader: [
                    'style-loader',
                    'css-loader?sourceMap&modules&localIdentName=[name]__[local]___[hash:base64:5]',
                    'postcss-loader',
                    'sass-loader?outputStyle=expanded'
                ].join('!')
            }
        ]
    },

    resolve: {
        extensions: ['', '.js', '.jsx', '.css']
    },

    postcss: [
        require('autoprefixer'),
        require('postcss-nested')
    ]

};
