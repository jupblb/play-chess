import React from 'react';
import {
    AppBar
} from 'material-ui';

import InputPage from './input/InputPage';
import OutputPage from './output/OutputPage';

export default class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {data: undefined};
    }

    render() {
        return (
            <div>
                <AppBar title="Play-Chess"
                        showMenuIconButton={false}
                />
                {this.state.data == undefined
                    ? <InputPage process={data => this.setState({data: data})}/>
                    : <OutputPage data={this.state.data} refresh={() => this.setState({data: undefined})}/>}
            </div>
        );
    }

}