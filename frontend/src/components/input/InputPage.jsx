import React from 'react';
import {
    Slider,
    Paper,
    SelectField,
    RaisedButton
} from 'material-ui';

import Styles from './_InputPage.scss';

export default class InputPage extends React.Component {

    static propTypes = {
        process: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            width: 5,
            height: 5,
            kings: 0,
            queens: 0,
            bishops: 0,
            rooks: 0,
            knights: 0
        }
    }

    render() {
        return (
            <Paper className={Styles.paper}>
                <table className={Styles.table}>
                    <tbody>
                    <tr>
                        <td>Height:</td>
                        <td><Slider className={Styles.slider}
                                    defaultValue={0.5}
                                    step={0.10}
                                    onChange={(e, value) => this.setState({height: value*10})}
                                    name="height"
                        /></td>
                        <td>{this.state.height}</td>
                    </tr>
                    <tr>
                        <td>Width:</td>
                        <td><Slider className={Styles.slider}
                                    defaultValue={0.5}
                                    step={0.10}
                                    onChange={(e, value) => this.setState({width: value*10})}
                                    name="width"
                        /></td>
                        <td>{this.state.width}</td>
                    </tr>
                    <tr>
                        <td>
                            <hr className={Styles.line}/>
                        </td>
                    </tr>
                    <tr>
                        <td style={{paddingTop: "20px"}}>Kings:</td>
                        <td style={{paddingTop: "20px"}}><Slider className={Styles.slider}
                                                                 defaultValue={0}
                                                                 step={0.10}
                                                                 onChange={(e, value) => this.setState({kings: value*10})}
                                                                 name="kings"
                        /></td>
                        <td style={{paddingTop: "20px"}}>{this.state.kings}</td>
                    </tr>
                    <tr>
                        <td>Queens:</td>
                        <td><Slider className={Styles.slider}
                                    defaultValue={0}
                                    step={0.10}
                                    onChange={(e, value) => this.setState({queens: value*10})}
                                    name="queens"
                        /></td>
                        <td>{this.state.queens}</td>
                    </tr>
                    <tr>
                        <td>Rooks:</td>
                        <td><Slider className={Styles.slider}
                                    defaultValue={0}
                                    step={0.10}
                                    onChange={(e, value) => this.setState({rooks: value*10})}
                                    name="rooks"
                        /></td>
                        <td>{this.state.rooks}</td>
                    </tr>
                    <tr>
                        <td>Bishops:</td>
                        <td><Slider className={Styles.slider}
                                    defaultValue={0}
                                    step={0.10}
                                    onChange={(e, value) => this.setState({bishops: value*10})}
                                    name="bishops"
                        /></td>
                        <td>{this.state.bishops}</td>
                    </tr>
                    <tr>
                        <td>Knights:</td>
                        <td><Slider className={Styles.slider}
                                    defaultValue={0}
                                    step={0.10}
                                    onChange={(e, value) => this.setState({knights: value*10})}
                                    name="knights"
                        /></td>
                        <td>{this.state.knights}</td>
                    </tr>
                    <tr>
                        <td>
                            <hr className={Styles.line}/>
                        </td>
                    </tr>
                    <tr>
                        <td style={{paddingTop: "25px", paddingBottom: "50px"}}>
                            <RaisedButton label="Simulate"
                                          secondary={true}
                                          style={{width: "calc(85vw - 20px)", position: "absolute"}}
                                          onClick={this.simulate}
                            />
                        </td>
                    </tr>
                    </tbody>
                </table>
            </Paper>
        );
    }

    simulate = () => this.props.process({
        width: this.state.width,
        height: this.state.height,
        pieceSet: {
            kings: this.state.kings,
            queens: this.state.queens,
            bishops: this.state.bishops,
            rooks: this.state.rooks,
            knights: this.state.knights
        }
    });

};