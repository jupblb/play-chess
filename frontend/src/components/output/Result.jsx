import React from 'react';

import Styles from './_OutputPage.scss';

export default class OutputPage extends React.Component {

    static propTypes = {
        data: React.PropTypes.object.isRequired
    };

    render() {
        return (
            <div>
                <center>
                    <div className={Styles.calculationTime}>
                        Calculation time: {this.props.data.calculationTime/1000} seconds
                    </div>
                    <br/>
                    {this.showScenarios(this.props.data.height, this.props.data.width, this.props.data.scenarios)}
                </center>
            </div>
        );
    }

    showScenarios = (height, width, scenarios) => {
        return scenarios.map(scenario => this.showScenario(height, width, scenario));
    };

    showScenario = (height, width, scenario) => {
        let board = this._create2DArray(height, width);
        scenario.forEach(configuration => {
            board[configuration.heightPosition - 1][configuration.widthPosition - 1] = configuration.category
        });

        return [
            <table>
                <tbody className={Styles.tableBorder}>
                {board.map(row => <tr className={Styles.tableBorder}>
                    {row.map(cell => <td className={Styles.tableCell}>{cell}</td>)}
                </tr>)}
                </tbody>
            </table>, <br/>
        ];
    };

    _create2DArray = (height, width) => {
        let board = new Array(height);
        for (let i = 0; i < height; i++) {
            board[i] = new Array(width);
            for (let j = 0; j < width; j++) {
                board[i][j] = " ";
            }
        }
        return board;
    };

}