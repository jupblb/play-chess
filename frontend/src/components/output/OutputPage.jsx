import React from 'react';
import {
    CircularProgress
} from 'material-ui';
import $ from 'jquery';

import Result from './Result';

import Styles from './_OutputPage.scss';

const config = require("../../../package.json");

export default class OutputPage extends React.Component {

    static propTypes = {
        data: React.PropTypes.object.isRequired,
        refresh: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {serverData: undefined}
    }

    componentDidMount() {
        $.ajax({
            url: `http://${config.backendHost}:${config.backendPort}/simulate`,
            type: "POST",
            data: JSON.stringify(this.props.data),
            contentType: "application/json",
            success: (result) => this.setState({serverData: result}),
            error: (xhr, status, error) => {
                alert(xhr.responseText);
                this.props.refresh();
            }
        });
    }

    render() {
        return (
            <div>
                {this.state.serverData != undefined
                    ? <Result data={this.state.serverData} />
                    : <div className={Styles.centered}><CircularProgress size={3}/></div>}
            </div>
        );
    }

}