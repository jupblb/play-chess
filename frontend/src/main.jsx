import 'babel-core/polyfill';
import 'normalize.css/normalize.css';

import React from 'react';
import ReactDOM from 'react-dom';
import TapPlugin from 'react-tap-event-plugin';

import Main from './components/Main';

ReactDOM.render(<Main/>, document.getElementById('app'));