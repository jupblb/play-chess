package controllers

import models.{InputBoard, Output}
import models.PieceCategories._
import play.api.libs.concurrent.Promise
import play.api.libs.json.Json
import play.api.mvc.{Action, BodyParsers, Controller}
import simulator.Simulator

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class Simulation extends Controller {

  private val Timeout = 5.minutes

  private val simulator = new Simulator

  def simulate() = Action.async(BodyParsers.parse.json) { request =>
    request.body.validate[InputBoard].fold(
      error => Future.successful(BadRequest(error.toString())),
      inputBoard => simulateInput(inputBoard) map {
        case output: Output => Ok(Json.toJson(output))
        case timeout: String => InternalServerError(timeout)
      }
    )
  }

  private def simulateInput(inputBoard: InputBoard) = Future.firstCompletedOf(Seq(
    Promise.timeout("Timeout", Timeout),
    simulator.start(inputBoard.height, inputBoard.width, inputBoard.toPieceCategoryList)
  ))

}
