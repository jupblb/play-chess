package models

import play.api.libs.json.{JsString, JsValue, JsSuccess, Format}

object PieceCategories extends Enumeration {
  type PieceCategory = Value

  val King = Value("KING")
  val Queen = Value("QUEEN")
  val Bishop = Value("BISHOP")
  val Rook = Value("ROOK")
  val Knight = Value("KNIGHT")

  implicit val pieceCategoriesFormat = new Format[PieceCategory] {
    def reads(json: JsValue) = JsSuccess(PieceCategories.withName(json.as[String]))

    def writes(enum: PieceCategory) = JsString(enum.toString)
  }

}
