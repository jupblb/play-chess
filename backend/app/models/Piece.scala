package models

import play.api.libs.json.Json

case class Piece(category: PieceCategories.PieceCategory, heightPosition: Int, widthPosition: Int)

object Piece {
  implicit val pieceFormat = Json.format[Piece]
}