package models

import models.PieceCategories._
import play.api.libs.json.Json

case class PieceSet(kings: Int, queens: Int, bishops: Int, rooks: Int, knights: Int) {
  def toPieceCategoryList = List.fill(kings)(King) ::: List.fill(queens)(Queen) ::: List.fill(bishops)(Bishop) :::
    List.fill(rooks)(Rook) ::: List.fill(knights)(Knight)
}

case class InputBoard(height: Int, width: Int, pieceSet: PieceSet) {
  def toPieceCategoryList = pieceSet.toPieceCategoryList
}

object PieceSet {
  implicit val pieceSetFormat = Json.format[PieceSet]
}

object InputBoard {
  implicit val inputBoardJsonFormat = Json.format[InputBoard]
}