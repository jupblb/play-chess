package models

import play.api.libs.json.Json

case class Output(height: Int, width: Int, scenarios: List[Seq[Piece]], calculationTime: Int) {
  def isEmpty = scenarios.isEmpty
}

object Output {
  implicit val outputBoardFormat = Json.format[Output]
}