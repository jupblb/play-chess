package simulator

import models.PieceCategories._
import org.specs2.mutable._

class ChessBoardSpec extends Specification {

  "ChessBoard" should {

    "On empty initialization be empty" in {
      new ChessBoard(0, 0).toString must be empty
    }

    "On single field initialization have one empty field" in {
      new ChessBoard(1, 1).toString must beEqualTo("0")
    }

    "On added king to a 1x1 chess board have one piece" in {
      new ChessBoard(1, 1).simulatePieceAddition(0, 0, King) must beTrue
    }

    "On added king to a 1x2 chess board have one piece and one threatened field" in {
      val chessBoard = new ChessBoard(1, 2)
      chessBoard.simulatePieceAddition(0, 0, King) must beTrue
      chessBoard.simulatePieceAddition(0, 0, King) must beFalse
    }

    "On 2x2 Chessboard must have (0,0), (1,0), (0,1), (1,1) coordinates" in {
      val chessBoard = new ChessBoard(2, 2)
      chessBoard.getAllCoordinates must have size 4
      chessBoard.getAllCoordinates must contain((0, 0))
      chessBoard.getAllCoordinates must contain((1, 0))
      chessBoard.getAllCoordinates must contain((0, 1))
      chessBoard.getAllCoordinates must contain((1, 1))
    }

  }

}