package simulator

import models.PieceCategories._
import models._
import org.specs2.mutable._
import play.api.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._

class SimulatorSpec extends Specification {

  private val defaultTimeout = 15.minutes

  private val simulator = new Simulator

  "Simulator" should {

    "On XxY dimension input return XxY dimension output" in {
      Await.result(simulator.start(1, 2, List()).map { out =>
        out.height must beEqualTo(1)
        out.width must beEqualTo(2)
      }, defaultTimeout)
    }

    "on empty input return output with empty result" in {
      Await.result(simulator.start(1, 1, List()).map { out =>
        out.scenarios must have size 1
        out.scenarios must contain(Set.empty)
      }, defaultTimeout)
    }

    "on zero dimension input return empty output" in {
      Await.result(simulator.start(0, 0, List(King)).map { out =>
        out.isEmpty must beTrue
      }, defaultTimeout)
    }

    "on impossible configuration return empty output" in {
      Await.result(simulator.start(1, 1, List(King, King)).map { out =>
        out.isEmpty must beTrue
      }, defaultTimeout)
    }

    "on single piece on 1x1 board return one scenario" in {
      Await.result(simulator.start(1, 1, List(King)).map { out =>
        out.scenarios must have size 1
        out.scenarios must contain(List(new Piece(King, 1, 1)))
      }, defaultTimeout)
    }

    "on single piece on 2x2 board return four scenarios" in {
      Await.result(simulator.start(2, 2, List(King)).map { out =>
        out.scenarios must have size 4
        out.scenarios must contain(List(new Piece(King, 1, 1)))
        out.scenarios must contain(List(new Piece(King, 1, 2)))
        out.scenarios must contain(List(new Piece(King, 2, 1)))
        out.scenarios must contain(List(new Piece(King, 2, 2)))
      }, defaultTimeout)
    }

    "on 2x3 input containing one kings and one rook return four scenarios" in {
      Await.result(simulator.start(2, 3, List(King, Rook)).map { out =>
        out.scenarios must have size 4
        out.scenarios must contain(List(
          new Piece(King, 1, 1),
          new Piece(Rook, 2, 3)
        ))
        out.scenarios must contain(List(
          new Piece(King, 2, 1),
          new Piece(Rook, 1, 3)
        ))
        out.scenarios must contain(List(
          new Piece(King, 1, 3),
          new Piece(Rook, 2, 1)
        ))
        out.scenarios must contain(List(
          new Piece(King, 2, 3),
          new Piece(Rook, 1, 1)
        ))
      }, defaultTimeout)
    }

    "on 2x3 input containing one kings and one knight return four scenarios" in {
      Await.result(simulator.start(2, 3, List(King, Knight)).map { out =>
        out.scenarios must have size 4
        out.scenarios must contain(List(
          new Piece(King, 1, 1),
          new Piece(Knight, 1, 3)
        ))
        out.scenarios must contain(List(
          new Piece(King, 1, 3),
          new Piece(Knight, 1, 1)
        ))
        out.scenarios must contain(List(
          new Piece(King, 2, 1),
          new Piece(Knight, 2, 3)
        ))
        out.scenarios must contain(List(
          new Piece(King, 2, 3),
          new Piece(Knight, 2, 1)
        ))
      }, defaultTimeout)
    }

    "on 2x3 input containing two kings return four scenarios" in {
      Await.result(simulator.start(2, 3, List(King, King)).map { out =>
        out.scenarios must have size 4
        out.scenarios must contain(List(
          new Piece(King, 1, 1),
          new Piece(King, 1, 3)
        ))
        out.scenarios must contain(List(
          new Piece(King, 1, 1),
          new Piece(King, 2, 3)
        ))
        out.scenarios must contain(List(
          new Piece(King, 1, 3),
          new Piece(King, 2, 1)
        ))
        out.scenarios must contain(List(
          new Piece(King, 2, 1),
          new Piece(King, 2, 3)
        ))
      }, defaultTimeout)
    }

    "on 4x4 input containing two rooks and four knights return eight scenarios" in {
      Await.result(simulator.start(4, 4, List(Rook, Rook, Knight, Knight, Knight, Knight)).map { out =>
        out.scenarios must have size 8
      }, defaultTimeout)
    }

    "on eight queen problem return 92 scenarios" in {
      Await.result(simulator.start(8, 8, List(Queen, Queen, Queen, Queen, Queen, Queen, Queen, Queen)).map { out =>
        out.scenarios must have size 92
      }, defaultTimeout)
    }

    "on task description test return something" in {
      Await.result(simulator.start(7, 7, List(King, King, Queen, Queen, Bishop, Bishop, Knight)).map { out =>
        out.scenarios must have size 3063828
        out.calculationTime must beGreaterThan(0)
      }, defaultTimeout)
    }

  }

}