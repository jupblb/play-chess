package models


object PieceCategories extends Enumeration {
  type PieceCategory = Value

  val King = Value("KING")
  val Queen = Value("QUEEN")
  val Bishop = Value("BISHOP")
  val Rook = Value("ROOK")
  val Knight = Value("KNIGHT")

}
