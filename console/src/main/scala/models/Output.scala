package models

case class Output(height: Int, width: Int, scenarios: List[Seq[Piece]], calculationTime: Int) {
  def isEmpty = scenarios.isEmpty
}