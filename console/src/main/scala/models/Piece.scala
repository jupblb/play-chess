package models

case class Piece(category: PieceCategories.PieceCategory, heightPosition: Int, widthPosition: Int)
