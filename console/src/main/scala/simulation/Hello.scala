package simulation

import models.PieceSet
import simulator.Simulator

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object Hello {

  private val expectedInput = "Expected input: height width kings queens bishops rooks knights"

  private val simulator = new Simulator

  def main(args: Array[String]): Unit = {
    if (args.length != 7) {
      println(expectedInput)
    } else {
      val output = Await.result(simulator.start(
        args(0).toInt, args(1).toInt,
        new PieceSet(args(2).toInt, args(3).toInt, args(4).toInt, args(5).toInt, args(6).toInt).toPieceCategoryList
      ), Duration.Inf)
      println(s"Time: ${output.calculationTime / 1000} seconds")
      println(s"Size of output: ${output.scenarios.size}")
      println("Press enter key to present consecutive scenarios.")
      for (scenario <- output.scenarios) {
        if(System.in.read() == 113) return
        println(scenario)
      }
    }
  }

}
