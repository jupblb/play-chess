package simulator

import models.{Piece, Output}
import models.PieceCategories._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps

class Simulator {

  def start(height: Int, width: Int, pieces: List[PieceCategory]): Future[Output] = {
    val startTime = System.nanoTime()
    Future(simulate(new ChessBoard(height, width), pieces.toVector, Vector())).map(out =>
      new Output(height, width, out, getTimeDifference(startTime))
    )
  }

  private def simulate(chessBoard: ChessBoard, piecesToUse: Vector[PieceCategory], piecesUsed: Vector[Piece]): List[Vector[Piece]] =
    if (piecesToUse.nonEmpty) {
      chessBoard.getAllCoordinates flatMap {
        case (i: Int, j: Int) =>
          Option(canAddPiece(chessBoard, piecesUsed, piecesToUse.head, i, j)) filter identity flatMap { _ =>
            val newChessBoard = chessBoard.clone()
            Option(newChessBoard.simulatePieceAddition(i, j, piecesToUse.head)) filter identity flatMap {
              _ => Some(
                simulate(newChessBoard, piecesToUse.tail, piecesUsed :+ new Piece(piecesToUse.head, i + 1, j + 1))
              )
            }
          }
      } flatten
    } else {
      List(piecesUsed)
    }

  private def getTimeDifference(startTime: Long): Int = ((System.nanoTime() - startTime) / 1e6).toInt

  private def canAddPiece(chessBoard: ChessBoard, piecesUsed: Vector[Piece], piece: PieceCategory, i: Int, j: Int) =
    pieceHasNotBeenProcessed(piecesUsed, piece, i, j) && chessBoard.isCellEmpty(i, j)

  private def pieceHasNotBeenProcessed(piecesUsed: Vector[Piece], pieceCategory: PieceCategory, i: Int, j: Int) =
    !piecesUsed.exists(piece => piece.category == pieceCategory &&
      ((piece.heightPosition - 1 == i && piece.widthPosition - 1 > j) || piece.heightPosition - 1 > i)
    )

}
