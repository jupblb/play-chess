package simulator

import models.PieceCategories._

import scala.language.postfixOps

case class ChessBoard(height: Int, width: Int, private val boardArray: Array[Array[Byte]]) {

  def this(height: Int, width: Int) = this(height, width, Array.fill[Byte](height, width)(0))

  def getAllCoordinates = (for (i <- 0 until height; j <- 0 until width) yield (i, j)) toList

  def simulatePieceAddition(i: Int, j: Int, pieceCategory: PieceCategory): Boolean =
    pieceCategory match {
      case King => addKing(i, j)
      case Queen => addQueen(i, j)
      case Bishop => addBishop(i, j)
      case Rook => addRook(i, j)
      case Knight => addKnight(i, j)
    }

  def isCellEmpty(i: Int, j: Int): Boolean = boardArray(i)(j) == 0

  private def addKing(i: Int, j: Int): Boolean = occupyWithoutConflicts(i, j) &&
    threatWithoutConflicts(i + 1, j + 1) &&
    threatWithoutConflicts(i + 1, j) &&
    threatWithoutConflicts(i + 1, j - 1) &&
    threatWithoutConflicts(i, j + 1) &&
    threatWithoutConflicts(i, j - 1) &&
    threatWithoutConflicts(i - 1, j + 1) &&
    threatWithoutConflicts(i - 1, j) &&
    threatWithoutConflicts(i - 1, j - 1)

  private def addQueen(i: Int, j: Int): Boolean = occupyWithoutConflicts(i, j) &&
    !(1 until Math.max(height, width) exists (
      x => !(threatWithoutConflicts(i - x, j - x)
        && threatWithoutConflicts(i - x, j + x)
        && threatWithoutConflicts(i + x, j - x)
        && threatWithoutConflicts(i + x, j + x))
      )) &&
    !(0 until height exists (x => !(x == i || threatWithoutConflicts(x, j)))) &&
    !(0 until width exists (x => !(x == j || threatWithoutConflicts(i, x))))

  private def addBishop(i: Int, j: Int): Boolean = occupyWithoutConflicts(i, j) &&
    !(1 until Math.max(height, width) exists (
      x => !(threatWithoutConflicts(i - x, j - x)
        && threatWithoutConflicts(i - x, j + x)
        && threatWithoutConflicts(i + x, j - x)
        && threatWithoutConflicts(i + x, j + x))
      ))

  private def addRook(i: Int, j: Int): Boolean = occupyWithoutConflicts(i, j) &&
    !(0 until height exists (x => !(x == i || threatWithoutConflicts(x, j)))) &&
    !(0 until width exists (x => !(x == j || threatWithoutConflicts(i, x))))

  private def addKnight(i: Int, j: Int): Boolean = occupyWithoutConflicts(i, j) &&
    threatWithoutConflicts(i + 2, j + 1) &&
    threatWithoutConflicts(i + 2, j - 1) &&
    threatWithoutConflicts(i + 1, j + 2) &&
    threatWithoutConflicts(i + 1, j - 2) &&
    threatWithoutConflicts(i - 1, j + 2) &&
    threatWithoutConflicts(i - 1, j - 2) &&
    threatWithoutConflicts(i - 2, j + 1) &&
    threatWithoutConflicts(i - 2, j - 1)

  private def occupyWithoutConflicts(i: Int, j: Int): Boolean =
    if (i >= 0 && i < height && j >= 0 && j < width) {
      if (boardArray(i)(j) == 0) {
        boardArray(i)(j) = -1
        true
      } else false
    } else true

  private def threatWithoutConflicts(i: Int, j: Int): Boolean =
    if (i >= 0 && i < height && j >= 0 && j < width) {
      if (boardArray(i)(j) == 0 || boardArray(i)(j) == 1) {
        boardArray(i)(j) = 1
        true
      } else false
    } else true

  override def clone() = new ChessBoard(height, width, boardArray.map(_.clone()))

  override def toString = boardArray.map(_.map(_.toString).mkString(", ")).mkString("; ")

}