# Play-Chess

##### Description
The key purpose of this project is to find all unique configurations of a set of normal chess pieces on a chess board where none of the pieces is in a position to take any of the others. For this problem we have some constraints:

-   Board is of dimension N x M
-   Color of the piece does not matter
-   There are no pawns

Expected output contains number of unique configurations, their list and calculation time.

##### How to potato?

In order to run the project on your computer you must do several things:  
1. **Setup tools**   
    - Download and install *activator* tool from Play Framework website  
    - Download and install *Node* along with *npm* (it should be bundled)  
2. **Run!**  
    - Go to /frontend directory and run command`$ npm install && npm run server`  
    - Go to /backend directory and run command `$ activator run`  
3. **Go to** http://localhost:8000/


##### Design
This project is designed as REST web application with Play Framework on backend and React on frontend. User inputs the query on the website and send it via POST request. The server processes it, calculates output and sends back JSON with results.

##### FAQ
1. **Why no Akka?!**
Well, this is a Play app so you have some threading on request/response level. The algorithm is based on recursion that creates quite a huge tree. Obviously you can't assign a new thread on each recursion level (too many). You shouldn't create thread pool as queue of chess boards states will be too memory consuming.  

2. **Why frontend is uhm... "crappy"?**
I did not pay much attention to frontend. Perhaps if I had time to write it properly using TypeScript and Karma+Mocha it would be better. Instead I focused on backend (Scala <3) part.

3. **What if I'd like to give a bigger input?**
You can use terminal app. Just go to console directory and do `$ activator` and later `run height width kings queens bishops rooks knights`  


#### Tools
* [Play Framework]
* [React]
* [Node]
* [Babel]
* [Express]
* [Webpack]
* [Material UI]
* [jQuery]

[Play Framework]: <https://www.playframework.com/>
[React]: <https://facebook.github.io/react/>
[Node]: <https://nodejs.org/en/>
[Babel]: <https://babeljs.io/>
[Express]: <http://expressjs.com/>
[Webpack]: <https://webpack.github.io/>
[Material UI]: <http://material-ui.com/#/>
[jQuery]: <https://jquery.com/>